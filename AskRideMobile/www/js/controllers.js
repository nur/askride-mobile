angular.module('AskRide.controllers', [])

.controller('AppController', function($scope,AskRideConfig, $ionicModal, $timeout,$cordovaOauth, $http, $localStorage, $location) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.facebookLogin = function() {
    console.log('Doing login', $scope.loginData);
    $cordovaOauth.facebook("403198273214824", ["email", "user_website", "user_location", "public_profile"]).then(function(result) {
            console.log(result);
            console.log(result.email);
            $localStorage.accessTokenOrig = result.access_token;
            $http.get("https://graph.facebook.com/v2.2/me", { 
                params: { 
                    access_token: result.access_token, 
                    fields: "email,id,name,gender,location,website,picture,relationship_status", 
                    format: "json" }
            }).then(
                    function(profile) { console.log(profile);
                console.log('email',profile.data.email);
                        var req = {
                            method: 'POST',
                            url: AskRideConfig.ServiceRoot + '/auth/api/authenticate/facebook',
                            headers:{
                                'Content-Type': 'application/json',
                            },
                            data:{
                                'email': profile.data.email,
                                'info':{
                                    'accessToken':result.access_token,
                                    'expiresIn':parseInt(result.expires_in, 10)
                                }
                            }
                        };

                        $http(req).then(
                                    function(value){
                                        $localStorage.accessToken = value;
                                        console.log(value);
                                        $location.path("/profile");
                                    },
                                    function(value){
                                        console.log("failed to get token", value);
                                    }
                                ); 
                    }, 
                    function(error) {
                        alert("There was a problem getting your profile.  Check the logs for details.");
                        console.log(error);
                    }
            );           
           
        }, function(error) {
            alert("There was a problem signing in!  See the console for logs");
            console.log(error);
            $location.path('/default');
        });
    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})
.controller("ProfileController", function($scope, AskRideConfig,$http, $localStorage, $location) {
    $scope.init = function() {
        if($localStorage.hasOwnProperty("accessToken") === true) {
            var req = {
                method: 'GET',
                url: AskRideConfig.ServiceRoot + "/userExist",
                headers:{
                    'Content-Type': 'application/json',
                    'X-Auth-Token':$localStorage.accessToken
                }                
            };
            
            $http(req).then(
                    function(result) {
                        $scope.profileData = result.data;
                    }, 
                    function(error) {
                            alert("There was a problem getting your profile.  Check the logs for details.");
                            console.log(error);
                        }
                    );
            
        } else {
            alert("Not signed in");
            $location.path("/default");
        }
    };

})
.controller('DefaultController', function($scope, $stateParams) {
})
.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})
.controller('MapController',function($scope, $state, AskRideConfig, $localStorage, $cordovaGeolocation, $ionicLoading, $compile){
    /*******************
     * scope variables.*
     *******************/ 
    var markers = [['00000000',42.1673279,-87.845757],['686023461494046',42.1698964,-87.8465724],['686023461494045',42.1672961,-87.8431821]];
    var infoWindowContent = [['<div><a ng-click="clickTest()">0 Click me!</a></div>'],['<div><a ng-click="clickTest()">1 Click me!</a></div>'],['<div><a ng-click="clickTest()">2 Click me!</a></div>']];            
    /*var options = {timeout: 10000, enableHighAccuracy: true};        
    $cordovaGeolocation.getCurrentPosition(options).then(function(position){ 
          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); 
          var mapOptions = {
              center: latLng,
              zoom: 15,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
    }, function(error){
        console.log(error);
    }); */       
                
    /******************
     * scope methods. *
     ******************/     
    /**
     * method to initialize map.
     * 
     * @param {type} markers
     * @param {type} infoWindowContents
     * @returns {undefined}
     */
    var initializeGoogleMap = function (markers, infoWindowContents) {
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            streetViewControl:true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $scope.map = new google.maps.Map(document.getElementById("map"),mapOptions);       
        setMarkers(markers, infoWindowContents, bounds);
        var boundsListener = google.maps.event.addListener(($scope.map), 'bounds_changed', function(event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });        
    };
    
    /**
     * method to initialize all markers
     * 
     * @param {type} markers
     * @param {type} infoWindowContents
     * @param {type} bounds
     * @returns {undefined}
     */
    var setMarkers = function(markers, infoWindowContents, bounds){
        for( var i = 0; i < markers.length; i++ ) {
            setMarker(markers[i], infoWindowContents[i], bounds, i);
        }        

        google.maps.event.addDomListener(window, "resize", function() {
            var center = $scope.map.getCenter();
            google.maps.event.trigger($scope.map, "resize");
            $scope.map.setCenter(center);
        });

        // Automatically center the map fitting all markers on the screen
        $scope.map.fitBounds(bounds);        
    };
    
    /***
     * method to set a marker.
     * 
     * @param {type} markerInfo
     * @param {type} inforWindowContent
     * @param {type} bounds
     * @param {type} markerPos
     * @returns {undefined}
     */
    var setMarker  = function (markerInfo, inforWindowContent, bounds, markerPos){
        var position = new google.maps.LatLng(markerInfo[1], markerInfo[2]);
        bounds.extend(position);
        var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: position,
            map: $scope.map,
                title: markerInfo[0]
        });
        if(markerPos !== 0){
            var pinIcon = new google.maps.MarkerImage(
                    "img/city-car.svg",
                    new google.maps.Size(200, 200),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(0, 30),
                    new google.maps.Size(50, 50)
            );
            marker.setIcon(pinIcon);
        }
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                var infoWindow = new google.maps.InfoWindow();
                var contentString = inforWindowContent[0];
                var compiled = $compile(contentString)($scope);
                infoWindow.setContent(compiled[0]);
                infoWindow.open($scope.map, marker);
            };
        })(marker, markerPos));        
        // Add circle overlay and bind to marker
        /*var circle = new google.maps.Circle({
          map: $scope.map,
          radius: 100,    // 10 miles in metres
          fillColor: '#AA0000'
        });
        circle.bindTo('center', marker, 'position');    */    
    };
    
    /**
     * 
     * @returns {undefined}
     */
    $scope.centerOnMe = function() {
        if(!$scope.map) {
            return;
        }
        $scope.loading = $ionicLoading.show({
            content: 'Getting current location...',
            showBackdrop: false
        });
        navigator.geolocation.getCurrentPosition(function(pos) {
            $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
            initialize();
            $ionicLoading.hide();
        }, function(error) {
            alert('Unable to get location: ' + error.message);
        });
    };

    /**
     * 
     * @returns {undefined}
     */
    $scope.clickTest = function() {
        alert('Hi..');
    };
    /****************************
     * method invoking section. *
     ****************************/    
    initializeGoogleMap(markers, infoWindowContent);
    
})
.controller('PlaylistCtrl', function($scope, $stateParams) {
});
