angular.module('index_directory.controllers', [])

    
	.controller('EmployeeIndexCtrl', function ($scope, EmployeeService) {
        $scope.searchKey = "";

        $scope.clearSearch = function () {
            $scope.searchKey = "";
            findAllEmployees();
        }

        $scope.search = function () {
            EmployeeService.findByName($scope.searchKey).then(function (employees) {
                $scope.employees = employees;
            });
        }

        var findAllEmployees = function() {
            EmployeeService.findAll().then(function (employees) {
                $scope.employees = employees;
            });
        }

        findAllEmployees();

    })

    .controller('EmployeeDetailCtrl', function ($scope, $stateParams, EmployeeService) {
        EmployeeService.findById($stateParams.employeeId).then(function(employee) {
            $scope.employee = employee;
        });
    })

    .controller('EmployeeReportsCtrl', function ($scope, $stateParams, EmployeeService) {
        EmployeeService.findByManager($stateParams.employeeId).then(function(employees) {
            $scope.employees = employees;
        });
    })
	
	.controller('MyController', function($scope, $ionicModal) {
		$ionicModal.fromTemplateUrl('my-modal.html', function(modal) {
			$scope.modal = modal;
			$scope.showModal = function() {
				$scope.modal.show()
			}
			$scope.hideModal = function() {
				$scope.modal.hide()
			}
		}, 
		{
			scope: $scope, 
			animation: 'slide-in-up', 
			
		});
		
		$scope.searchId = null;
		$scope.callFunction=function(){
			
			alert('2. Value :: '+$scope.searchId);
		}
	});